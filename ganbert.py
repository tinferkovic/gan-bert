import torch
import numpy as np
import torch.nn.functional as F
from torch import nn
from transformers import BertConfig, BertModel, BertTokenizer
from torch.utils.data import RandomSampler, SequentialSampler, DataLoader, Dataset
from sklearn.metrics import f1_score, accuracy_score
import time
import datetime
import os

label_list = ["UNK_UNK", "ABBR_abb", "ABBR_exp", "DESC_def", "DESC_desc",
              "DESC_manner", "DESC_reason", "ENTY_animal", "ENTY_body",
              "ENTY_color", "ENTY_cremat", "ENTY_currency", "ENTY_dismed",
              "ENTY_event", "ENTY_food", "ENTY_instru", "ENTY_lang",
              "ENTY_letter", "ENTY_other", "ENTY_plant", "ENTY_product",
              "ENTY_religion", "ENTY_sport", "ENTY_substance", "ENTY_symbol",
              "ENTY_techmeth", "ENTY_termeq", "ENTY_veh", "ENTY_word", "HUM_desc",
              "HUM_gr", "HUM_ind", "HUM_title", "LOC_city", "LOC_country",
              "LOC_mount", "LOC_other", "LOC_state", "NUM_code", "NUM_count",
              "NUM_date", "NUM_dist", "NUM_money", "NUM_ord", "NUM_other",
              "NUM_perc", "NUM_period", "NUM_speed", "NUM_temp", "NUM_volsize",
              "NUM_weight"]
label_dict = dict((j, i) for i, j in enumerate(label_list))

def format_seconds(seconds):
    td = datetime.timedelta(seconds=int(round(seconds)))
    return str(td)

model_name = 'bert-base-cased'
tokenizer = BertTokenizer.from_pretrained(model_name)
config = BertConfig.from_pretrained(model_name)
max_length_tokenization = 64
batch_size = 64
num_epochs = 10
log_stabilization_const = 1e-8
use_schedulers = True
print_every_n_batches = 10

generator_input_size = 100
generator_hidden_layer_sizes = [768]
lr_g = 1e-5
generator_leaky_relu_negative_slope = 0.2
generator_dropout = 0.1
generator_exponential_lr_scheduler_gamma = 0.9

discriminator_hidden_layer_sizes = [768]
lr_d = 1e-5
discriminator_leaky_relu_negative_slope = 0.2
discriminator_dropout = 0.1
discriminator_exponential_lr_scheduler_gamma = 0.9

os.environ["CUDA_VISIBLE_DEVICES"] = "1"
if torch.cuda.is_available():
    device = torch.device('cuda:0')
    print('Using CUDA for training')
else:
    device = torch.device('cpu')
    print('Using CPU for training')


class CustomDataset(Dataset):
    def __init__(self, encoded_inputs, input_masks, labels, label_masks):
        self.encoded_inputs = encoded_inputs
        self.input_masks = input_masks
        self.labels = labels
        self.label_masks = label_masks

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        return self.encoded_inputs[idx], self.input_masks[idx], self.labels[idx], self.label_masks[idx]

# define generator


class Generator(nn.Module):
    '''
    Generator which attepts to mock as good as possible last hidden layer's output from BERT in order to deceive a discriminator.
    '''

    def __init__(self, input_size, hidden_sizes, output_size, leaky_relu_neg_slope, dropout):
        super().__init__()

        layers = []
        layers.extend([nn.Linear(input_size, hidden_sizes[0]), nn.LeakyReLU(
            negative_slope=leaky_relu_neg_slope, inplace=True), nn.Dropout(dropout)])
        for i in range(len(hidden_sizes) - 1):
            layers.extend([nn.Linear(hidden_sizes[i], hidden_sizes[i+1]), nn.LeakyReLU(
                negative_slope=leaky_relu_neg_slope, inplace=True), nn.Dropout(dropout)])
        layers.append(nn.Linear(hidden_sizes[-1], output_size))
        self.layers = nn.Sequential(*layers)

    def forward(self, random_input):
        return self.layers(random_input)

# define discriminator


class Discriminator(nn.Module):
    '''
    Discriminator (D) which attempts to differentiate between actual examples and the ones from the Generator (G).
    Also tries to classify labeled examples correctly.
    '''

    def __init__(self, input_size, hidden_sizes, num_labels, leaky_relu_neg_slope, dropout):
        super().__init__()

        layers = []
        layers.append(nn.Dropout(dropout))
        layers.extend([nn.Linear(input_size, hidden_sizes[0]), nn.LeakyReLU(
            negative_slope=leaky_relu_neg_slope, inplace=True), nn.Dropout(dropout)])
        for i in range(len(hidden_sizes) - 1):
            layers.extend([nn.Linear(hidden_sizes[i], hidden_sizes[i+1], nn.LeakyReLU(
                negative_slope=leaky_relu_neg_slope, inplace=True), nn.Dropout(dropout))])
        # important to separate the features as we want to compare how similar they are in real vs generated examples and contribute to the discriminator loss in that way
        self.features = nn.Sequential(*layers)
        # +1 since we want to have an additional label to differantiate between real and fake examples
        self.logits = nn.Linear(hidden_sizes[-1], num_labels + 1)
        self.probs = nn.Softmax(dim=-1)

    def forward(self, input):
        features = self.features(input)
        logits = self.logits(features)
        probs = self.probs(logits)
        return features, logits, probs


def get_examples(file):
    examples = []
    with open(file, "r") as f:
        for line in f.readlines()[1:]:
            label = line.split('\t', maxsplit=1)[0].strip()
            text = line.split('\t', maxsplit=1)[1].strip()
            examples.append((text, label.replace(':', '_')))
    return examples


def generate_data_loader(examples, label_masks, label_dict, max_length_tokenization, batch_size=64, shuffle=True):
    #percent_labeled = sum(label_masks) / len(label_masks)

    encoded_inputs = []
    input_masks = []
    labels = []
    for example in examples:
        input = example[0]
        label = label_dict[example[1]]

        encoded_input = tokenizer.encode(
            input, padding='max_length', truncation=True, max_length=max_length_tokenization)

        input_mask = [int(i != 0) for i in encoded_input]

        encoded_inputs.append(encoded_input)
        input_masks.append(input_mask)
        labels.append(label)

    dataset = CustomDataset(torch.tensor(encoded_inputs),
                            torch.tensor(input_masks), torch.tensor(labels), torch.tensor(label_masks).bool())

    if shuffle:
        sampler = RandomSampler(dataset)
    else:
        sampler = SequentialSampler(dataset)

    return DataLoader(dataset, batch_size, sampler=sampler)


def training(percentage_labeled):
    bert = BertModel.from_pretrained(model_name)

    dir_name = 'labeled_' + str(percentage_labeled) + '_percent'
    labeled_file = './data_grained_percentwise/' + dir_name + '/labeled.tsv'
    unlabeled_file = './data_grained_percentwise/' + dir_name + '/unlabeled.tsv'
    test_file = './data_grained_percentwise/' + dir_name + '/test.tsv'

    labeled_examples = get_examples(labeled_file)
    unlabeled_examples = get_examples(unlabeled_file)
    test_examples = get_examples(test_file)

    train_examples = labeled_examples
    train_label_masks = np.ones(len(labeled_examples), dtype=np.int8)

    if len(unlabeled_examples) > 0:
        train_examples.extend(unlabeled_examples)
        train_label_masks = np.append(train_label_masks, np.zeros(
            len(unlabeled_examples), dtype=np.int8))

    train_data_loader = generate_data_loader(
        train_examples, train_label_masks, label_dict, max_length_tokenization, batch_size, shuffle=True)

    test_label_masks = np.zeros(len(test_examples), dtype=np.int8)
    test_data_loader = generate_data_loader(
        test_examples, test_label_masks, label_dict, max_length_tokenization, batch_size, shuffle=False)


    generator = Generator(generator_input_size, generator_hidden_layer_sizes,
                        config.hidden_size, generator_leaky_relu_negative_slope, generator_dropout)

    discriminator = Discriminator(config.hidden_size, discriminator_hidden_layer_sizes, len(
        label_dict), discriminator_leaky_relu_negative_slope, discriminator_dropout)

    bert = bert.to(device)
    generator = generator.to(device)
    discriminator = discriminator.to(device)

    # define optimizers
    optimizer_g = torch.optim.AdamW(generator.parameters(), lr_g)

    discriminator_parameters = []
    discriminator_parameters.extend(list(bert.parameters()))
    discriminator_parameters.extend(list(discriminator.parameters()))

    optimizer_d = torch.optim.AdamW(discriminator_parameters, lr_d)

    # define schedulers
    if use_schedulers:
        scheduler_g = torch.optim.lr_scheduler.ExponentialLR(
            optimizer_g, gamma=generator_exponential_lr_scheduler_gamma)
        scheduler_d = torch.optim.lr_scheduler.ExponentialLR(
            optimizer_d, gamma=discriminator_exponential_lr_scheduler_gamma)

    output_dictionaries = []

    t0 = time.time()

    print('='*20 + f' TRAINING GANBERT FOR {percentage_labeled}% LABELED ' + '='*20)
    print('\n')

    # training...
    for epoch in range(num_epochs):

        epoch_train_t0 = time.time()

        print('='*20 + f' EPOCH {epoch + 1} ' + '='*20)
        print('\n')
        print('='*10 + ' TRAINING ' + '='*10)
        print('\n')

        epoch_loss_train_generator = 0
        epoch_loss_train_discriminator = 0

        bert.train()
        generator.train()
        discriminator.train()

        for i, (encoded_inputs, input_masks, labels, label_masks) in enumerate(train_data_loader):

            encoded_inputs = encoded_inputs.to(device)
            input_masks = input_masks.to(device)
            labels = labels.to(device)
            label_masks = label_masks.to(device)

            if i % print_every_n_batches == 0 and i != 0:
                print(
                    f'Batch {i:>5} of {len(train_data_loader):>5}. Elapsed: {format_seconds(time.time() - epoch_train_t0)}')

            bert_output = bert(input_ids=encoded_inputs,
                            attention_mask=input_masks)
            # last layer hidden-state of the CLS token after processing through linear layer and tanh
            bert_output = bert_output.pooler_output

            # can differ from predefined batch_size if there's not enough data at the end
            current_batch_size = encoded_inputs.shape[0]

            noise = torch.randn((current_batch_size, generator_input_size), device=device)
            fake_output = generator(noise)  # same size as bert_output

            input_discriminator = torch.cat([bert_output, fake_output], dim=0)

            features, logits, probs = discriminator(input_discriminator)
            features_split = torch.split(features, current_batch_size)
            logits_split = torch.split(logits, current_batch_size)
            probs_split = torch.split(probs, current_batch_size)

            features_real = features_split[0]
            features_fake = features_split[1]
            probs_real = probs_split[0]
            probs_fake = probs_split[1]
            logits_real = logits_split[0]
            logits_fake = logits_split[1]

            # generator loss
            loss_g_feature_matching = torch.mean(torch.pow(torch.mean(
                features_real, dim=0) - torch.mean(features_fake, dim=0), 2))
            loss_g_unsupervised = - \
                torch.mean(
                    torch.log(1 - probs_fake[:, -1] + log_stabilization_const))
            loss_g = loss_g_feature_matching + loss_g_unsupervised

            # discriminator loss
            log_probs = torch.log_softmax(logits_real[:, :-1], dim=-1)
            labels_one_hot = F.one_hot(labels, num_classes=len(label_list))
            # list of losses per each example
            example_losses = -torch.sum(log_probs * labels_one_hot, dim=-1)
            example_losses = torch.masked_select(example_losses, label_masks)
            labeled_count = len(example_losses)

            if labeled_count == 0:
                loss_d_supervised = 0
            else:
                loss_d_supervised = torch.sum(example_losses) * 1/labeled_count

            loss_d_unsupervised_real = - \
                torch.mean(
                    torch.log(1 - probs_real[:, -1] + log_stabilization_const))
            loss_d_unsupervised_fake = - \
                torch.mean(torch.log(probs_fake[:, -1] + log_stabilization_const))

            loss_d = loss_d_supervised + loss_d_unsupervised_real + loss_d_unsupervised_fake

            optimizer_g.zero_grad()
            optimizer_d.zero_grad()

            loss_g.backward(retain_graph=True)
            loss_d.backward()

            optimizer_g.step()
            optimizer_d.step()

            epoch_loss_train_generator += loss_g.item()
            epoch_loss_train_discriminator += loss_d.item()

        if use_schedulers:
            scheduler_g.step()
            scheduler_d.step()

        # average epoch train loss
        average_epoch_loss_train_generator = epoch_loss_train_generator / \
            len(train_data_loader)
        average_epoch_loss_train_discriminator = epoch_loss_train_discriminator / \
            len(train_data_loader)
        epoch_train_duration = time.time() - epoch_train_t0

        print('\n')
        print(
            f'Average generator loss on train: {average_epoch_loss_train_generator:.4f}')
        print(
            f'Average discriminator loss on train: {average_epoch_loss_train_discriminator:.4f}')
        print(f'Epoch training time: {format_seconds(epoch_train_duration)}')
        print('\n')

        # evaluation
        epoch_test_t0 = time.time()
        print('='*10 + ' EVALUATING ' + '='*10)
        epoch_loss_test_discriminator = 0
        y_true = []
        y_pred = []

        bert.eval()
        generator.eval()
        discriminator.eval()

        cross_entropy_loss = torch.nn.CrossEntropyLoss()

        with torch.no_grad():
            for encoded_input, input_masks, labels, label_masks in test_data_loader:
                encoded_input = encoded_input.to(device)
                input_masks = input_masks.to(device)
                labels = labels.to(device)
                label_masks = label_masks.to(device)
                
                model_output = bert(input_ids=encoded_input,
                                    attention_mask=input_masks)
                model_output = model_output.pooler_output

                current_batch_size = encoded_inputs.shape[0]

                _, logits, _ = discriminator(model_output)
                logits = logits[:, :-1]

                epoch_loss_test_discriminator += cross_entropy_loss(logits, labels)
                
                y_pred.extend(torch.max(logits, dim=-1).indices.detach().cpu())
                y_true.extend(labels.detach().cpu())

        average_epoch_loss_test_discriminator = epoch_loss_test_discriminator / \
            len(test_data_loader)

        y_true = torch.stack(y_true).numpy()
        y_pred = torch.stack(y_pred).numpy()

        test_accuracy = accuracy_score(y_true, y_pred)
        test_f1_score = f1_score(y_true, y_pred, average='micro')
        epoch_test_duration = time.time() - epoch_test_t0

        print('\n')
        print(f'Validation accuracy: {test_accuracy:.4f}')
        print(f'Validation f1-score: {test_f1_score:.4f}')
        print(
            f'Average discriminator loss on test: {average_epoch_loss_test_discriminator:.4f}')
        print(f'Epoch validating time: {format_seconds(epoch_test_duration)}')
        print('\n')

        output_dictionaries.append(
            {
                'percent_labeled': percentage_labeled,
                'epoch': epoch + 1,
                'validation accuracy': test_accuracy,
                'validation f1-score': test_f1_score,
                'average discriminator loss on test': average_epoch_loss_test_discriminator,
                'average discirminator loss on train': average_epoch_loss_train_discriminator,
                'average generator loss on train': average_epoch_loss_train_generator,
                'time spent training': format_seconds(epoch_train_duration),
                'time spent validating': format_seconds(epoch_test_duration)
            }
        )

    with open('log2.txt', 'a') as f:
        for dict in output_dictionaries:
            print(dict)
            print()
            f.write(str(dict) + '\n')
        f.write('\n')

    print(f'Finished! Training took {format_seconds(time.time() - t0)}')

percentages = [1, 2, 5, 10, 20, 30, 40, 50]
for p in percentages:
    training(p)
